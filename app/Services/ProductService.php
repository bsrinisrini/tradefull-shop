<?php

namespace App\Services;

use GuzzleHttp\Client;
use Illuminate\Cache\Repository;
use Illuminate\Http\Request as IlluminateRequest;
use Illuminate\Http\Response;
use Illuminate\Support\Str;

/**
 * Service to interact with the product Service.
 * Class ProductService
 * @package App\Services
 */
class ProductService
{
    /**
     * @var string $endPoint
     */
    protected $endPoint;

    /**
     * @var int $timeout
     */
    protected $timeout;

    /**
     * @var array $defaultHeaders
     */
    protected $defaultHeaders;

    /**
     * @var Client $client
     */
    protected $client;

    /**
     * ProductService constructor.
     * @param Client $client
     * @param Repository $cacheRepository
     */
    public function __construct(Client $client, Repository $cacheRepository)
    {
        $atlasConfig = config('product');
        $this->endPoint = $atlasConfig['endpoint'] . '/product';
        $this->timeout = $atlasConfig['timeout'];
        $this->defaultHeaders = ['Accept' => 'application/json'];

        $this->client = $client;
    }

    /**
     * @param string $specialty
     * @return array|null
     */
    public function saveProduct(array $product): bool
    {
        unset($product['id']);
        try {
            $this->client->request("POST", $this->endPoint . '/store', [
                'form_params' => $product]);
        }catch(\Exception $ex) {
            echo 'Exception: '.$ex->getMessage();
            return false;
        }
        return true;
    }

    public function getProducts(): array
    {
        try {
            $res = $this->client->request("GET", $this->endPoint);
            $status = $res->getStatusCode();
            //$res->getHeader('content-type');
            if ($status == 200) {
                $data = json_decode($res->getBody()->getContents(), true);
            } else {
                $data = [];
            }
            return $data;
        }catch (\Exception $ex) {
            echo 'Exception: '. $ex->getMessage();
            exit;
        }
    }

    /**
     * We search Topics on Avvo with the SpecialtyId and Slug
     * on Atlas and return the first.
     *
     * @param int $specialtyId
     * @param string $slug
     * @return array
     */
    public function findProductById($productId): array
    {
        $params = [
            'specialty_id' => $specialtyId,
            'q' => $slug,
            'page' => 1,
        ];
        $uri = $this->endPoint . '/topics?' . http_build_query($params);
        $response = $this->client->request(IlluminateRequest::METHOD_GET, $uri);

        $response = Json::decode($response->getBody()->getContents(), true);

        return $response['hits'][0];
    }

}
