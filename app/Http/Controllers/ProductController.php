<?php

namespace App\Http\Controllers;

use App\Services\ProductService;
use GrantsClient\Facades\LawyerGrants;
use Illuminate\Http\Request;
use Swis\JsonApi\Client\Item;

/**
 * LegalAnswerController class
 *
 * Controller to communicate with Sequoia and response
 * with content parsed accordingly to the request made.
 *
\ */
class ProductController extends Controller
{
    protected $productService;

    public function __construct(
        ProductService $productService
    ) {
        $this->productService = $productService;
    }

    public function getProducts()
    {
        $data = $this->productService->getProducts();
        $metaData = [
            'title' => 'Product List',
            'desc' => 'Product List displaying from service',
            'no_index' => true
        ];

        return view('products', [
            'products' => $data,
            'data' => $metaData
        ]);
    }

    public function addProduct()
    {
        $metaData = [
            'title' => 'Add Product',
            'desc' => 'Add product into database',
            'no_index' => true
        ];
        return view('add-product', [
            'data' => $metaData
        ]);
    }

    public function storeProduct(Request $request)
    {
        $productData = $request->post();
        $data = $this->productService->saveProduct($productData);
        return redirect('/product', 301);
    }
}
