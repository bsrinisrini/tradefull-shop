<?php

return [
    'endpoint' => rtrim(env('SERVICE_URL', ''), '/'),
    'timeout' => 60 // seconds
];
