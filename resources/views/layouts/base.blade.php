<!DOCTYPE html>
<html lang="en">
    <head>
    @section('meta')
        @include('layouts.common.metaTags')
    @show
    @section('headStyles')
        <link rel='stylesheet' id='bridge-qode-font_awesome_5-css' href='https://tradefull.com/wp-content/themes/bridge/css/font-awesome-5/css/font-awesome-5.min.css?ver=6.0.2' type='text/css' media='all' />
        <link rel='stylesheet' id='bridge-stylesheet-css' href='https://tradefull.com/wp-content/themes/bridge/css/stylesheet.min.css?ver=6.0.2' type='text/css' media='all' />
        <style>
            .header {
                background-color: #15B9A9 !important;
                color: #fff;
                height: 100px;
                padding: 10px 20px;
            }
            .footer_top_holder .qode-cf-newsletter-outer {
                display: flex;
                align-items: flex-end;
            }

            .footer_top_holder .qode-cf-newsletter-submit input[type="submit"] {
                padding: 0;
                height: auto;
                line-height: 1em;
                position: relative;
                top: 3px;
                will-change: transform;
            }

            .footer_top_holder .qode-cf-newsletter-mail input {
                padding-bottom: 5px !important;
            }

            .footer_top_holder .qode-cf-newsletter-submit .ajax-loader {
                position: absolute;
            }

            .footer_top_holder .qode-cf-newsletter-submit {
                margin-left: 12px;
                position: relative;
            }

            .footer_top_holder .qode-cf-newsletter-submit:before,
            .footer_top_holder .qode-cf-newsletter-submit:after {
                content: '';
                width: 100%;
                height: 1px;
                background-color: #ff2c2c;
                position: absolute;
                bottom: 1px;
                left: 0;
            }

            .footer_top_holder .qode-cf-newsletter-submit:before {
                transform: scaleX(0);
                transform-origin: 0 0;
                transition: transform .4s cubic-bezier(.59,.02,.17,.95);
            }

            .footer_top_holder .qode-cf-newsletter-submit:after {
                transform: scaleX(1);
                transform-origin: 100% 0;
                transition: transform .4s cubic-bezier(.59,.02,.17,.95) .2s;
            }

            .footer_top_holder .qode-cf-newsletter-submit:hover:before {
                transform: scaleX(1);
                transition: transform .4s cubic-bezier(.59,.02,.17,.95) .2s;
            }

            .footer_top_holder .qode-cf-newsletter-submit:hover:after {
                transform: scaleX(0);
                transition: transform .4s cubic-bezier(.59,.02,.17,.95);
            }

            .footer_top .four_columns .column4 .column_inner>div {
                margin: 0;
            }

            .footer_bottom {
                text-align: left;
            }
            .footer_top, .footer_top p, .footer_top span, .footer_top li, .footer_top .textwidget, .footer_top .widget_recent_entries > ul > li > span {
                color: #fff;
            }
            .footer_top a:not(.qbutton) {
                color: #fff !important;
            }
            .footer_bottom, .footer_bottom span, .footer_bottom p, .footer_bottom p a, .footer_bottom a, #lang_sel_footer ul li a, footer #lang_sel > ul > li > a, footer #lang_sel_click > ul > li > a, footer #lang_sel a.lang_sel_sel, footer #lang_sel_click a.lang_sel_sel, footer #lang_sel ul ul a, footer #lang_sel_click ul ul a, footer #lang_sel ul ul a:visited, footer #lang_sel_click ul ul a:visited, footer #lang_sel_list.lang_sel_list_horizontal a, footer #lang_sel_list.lang_sel_list_vertical a, #lang_sel_footer a, .footer_bottom ul li a {
                color: #fff;
            }
            .in_grid {
                width: 1100px;
                margin: 0 auto;
            }
        </style>
            <link rel='stylesheet' id='bridge-stylesheet-css' href='/css/app.css' type='text/css' media='all' />
    @show
</head>
<body>
    <div class="main-wrapper">
        @include('layouts.common.header')
        <div class="content in_grid">
            @section('content')
            @show
        </div>
        @include('layouts.common.footer')
    </div>
</body>
</html>
