<footer>
    <div class="footer_inner clearfix">
        <div class="footer_top_holder">
            <div class="footer_top">
                <div class="container">
                    <div class="container_inner">
                        <div class="four_columns clearfix">
                            <div class="column1 footer_col1">
                                <div class="column_inner">
                                    <div id="block-7" class="widget widget_block">
                                        <div class="wp-container-3 wp-block-columns">
                                            <div class="wp-container-2 wp-block-column" style="flex-basis:100%">
                                                <div class="wp-container-1 wp-block-group"><div class="wp-block-group__inner-container">
                                                        <p class="has-custom-weight" style="font-weight:bold">Company</p>
                                                        <p><a href="https://tradefull.com/about/">About</a></p>
                                                        <p><a href="https://tradefull.com/contact/" data-type="URL">Contact</a></p>
                                                    </div></div>
                                            </div>
                                        </div>
                                    </div> </div>
                            </div>
                            <div class="column2 footer_col2">
                                <div class="column_inner">
                                    <div id="block-10" class="widget widget_block widget_text">
                                        <p class="has-custom-weight" style="font-weight:bold">Services</p>
                                    </div><div id="block-11" class="widget widget_block widget_text">
                                        <p><a href="https://tradefull.com/services/dropshipping/">Dropshipping</a></p>
                                    </div><div id="block-13" class="widget widget_block widget_text">
                                        <p><a href="https://tradefull.com/services/warehousing-fulfillment/">Warehousing</a></p>
                                    </div><div id="block-14" class="widget widget_block widget_text">
                                        <p><a href="https://tradefull.com/services/fulfillment/">Fulfillment</a></p>
                                    </div><div id="block-15" class="widget widget_block widget_text">
                                        <p><a href="https://tradefull.com/services/marketplaces/">Marketplaces</a></p>
                                    </div> </div>
                            </div>
                            <div class="column3 footer_col3">
                                <div class="column_inner">
                                    <div id="block-19" class="widget widget_block widget_text">
                                        <p><strong>Resources</strong></p>
                                    </div><div id="block-18" class="widget widget_block widget_text">
                                        <p><a href="https://tradefull.com/blog/">Blog</a></p>
                                    </div><div id="block-25" class="widget widget_block widget_text">
                                        <p><a href="https://tradefull.com/what-is-3pl/">3PL Guide</a></p>
                                    </div><div id="block-26" class="widget widget_block widget_text">
                                        <p><a href="https://tradefull.com/ecommerce-solutions/" data-type="page" data-id="2458">E-Commerce Solutions Guide</a></p>
                                    </div><div id="block-16" class="widget widget_block widget_text">
                                        <p>Support</p>
                                    </div> </div>
                            </div>
                            <div class="column4 footer_col4">
                                <div class="column_inner">
                                    <div id="block-24" class="widget widget_block widget_text">
                                        <p><strong>Contact Us</strong></p>
                                    </div><div id="block-23" class="widget widget_block widget_text">
                                        <p>2100 International Pkwy,</p>
                                    </div><div id="block-22" class="widget widget_block widget_text">
                                        <p>North Canton, OH 44720</p>
                                    </div><div id="block-21" class="widget widget_block widget_text">
                                        <p>1-888-203-0826</p>
                                    </div><span class="q_social_icon_holder normal_social"><a itemprop="url" href="https://www.facebook.com/TradefullHQ" target="_blank"><i class="qode_icon_font_awesome fa fa-facebook-official  simple_social" style=""></i></a></span><span class="q_social_icon_holder normal_social"><a itemprop="url" href="https://twitter.com/tradefullhq" target="_blank"><i class="qode_icon_font_awesome fa fa-twitter  simple_social" style=""></i></a></span> </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer_bottom_holder">
            <div style="background-color: #707070;height: 1px" class="footer_bottom_border in_grid"></div>
            <div class="container">
                <div class="container_inner">
                    <div class="footer_bottom">
                        <div class="textwidget"><p class="qode-footer-copyright">Copyright @ Tradefull 2022 | <a href="https://tradefull.com/privacy-policy/">Privacy Policy</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
