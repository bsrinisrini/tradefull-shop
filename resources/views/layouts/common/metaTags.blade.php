<meta charset="UTF-8"/>
<meta http-equiv="X-UA-Compatible" content="ie=edge,chrome=1"/>
<meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1"/>

<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="description" content="@yield('metaDescription')"/>
<link rel="icon" href="https://tradefull.com/wp-content/uploads/2021/09/favicon-150x150.png"/>
@if (!empty($data['no_index']) && $data['no_index'] == true)
    <meta name="robots" content="noindex,follow"/>
@else
    <meta name="robots" content="index,follow"/>
@endif
<title>@yield('title')</title>
