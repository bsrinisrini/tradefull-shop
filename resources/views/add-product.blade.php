@extends('layouts.base')

@section('title', 'Tradefull - '.$data['title'])
@section('metaDescription', substr(strip_tags($data['desc']), 0, 201))

@section('content')
    <div class="form-container">
        <form action="/product/store" method="post" class="wpcf7-form" enctype='multipart/form-data'>
            @csrf
            <div style="display: none;">
                <input type="hidden" name="id" value="">
            </div>
            <label> Product Name
                <span class="wpcf7-form-control-wrap"><input type="text" name="name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"></span> </label>
            <label> Price
                <span class="wpcf7-form-control-wrap"><input type="text" name="price" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required"></span> </label>
            <label> Image Path
                <span class="wpcf7-form-control-wrap"><input type="text" name="image_path" value="" size="40" class="wpcf7-form-control wpcf7-text"></span> </label>
            <label> Description
                <span class="wpcf7-form-control-wrap"><textarea name="description" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea"></textarea></span> </label>
            <input type="submit" value="Submit" class="wpcf7-form-control has-spinner wpcf7-submit"><span class="wpcf7-spinner"></span><div class="wpcf7-response-output" aria-hidden="true"></div>
        </form>
    </div>
@stop
