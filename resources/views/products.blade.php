@extends('layouts.base')

@section('title', 'Tradefull - '.$data['title'])
@section('metaDescription', substr(strip_tags($data['desc']), 0, 201))

@section('content')
    <section class="u-align-center u-clearfix u-section-2" id="sec-6156">
        <div class="u-clearfix u-sheet u-sheet-1">
            <h2 class="u-custom-font u-font-ubuntu u-text u-text-default u-text-1">Products List</h2>
            <div class="u-border-3 u-border-grey-dark-1 u-line u-line-horizontal u-line-1"></div>
            <div class="u-expanded-width u-list u-repeater u-list-1">
                @foreach($products as $product)
                <div class="u-container-style u-list-item u-repeater-item">
                    <div class="u-container-layout u-similar-container u-valign-bottom u-container-layout-1">
                        <img src="{{$product['image_path']}}" alt="" class="u-expanded-width u-image u-image-default u-image-1" data-image-width="1900" data-image-height="2532">
                        <h5 class="u-custom-font u-font-ubuntu u-text u-text-default u-text-2">{{$product['name']}}</h5>
                        <h3 class="u-custom-font u-font-ubuntu u-text u-text-default u-text-3">$ {{$product['price']}}</h3>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>
@stop
